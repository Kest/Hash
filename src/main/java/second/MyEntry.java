package second;

public class MyEntry<K, V> {
    K key;
    V value;

    public MyEntry() {
    }

    public MyEntry(K key, V value) {
        this.key = key;
        this.value = value;
    }

    MyEntry<K, V> next = null;
    MyEntry<K, V> previous = null;
}
