package second;

public interface Hasher<T> {
    Integer hash(T from);
}
