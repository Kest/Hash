package second;

import java.util.*;

public class MyHashTable<K, V> {
    int defaultSize = 16;
    private MyEntry<K, V>[] items;
    int size;
    int load;

    Hasher<K> hasher;

    public MyHashTable() {
        hasher = (key) -> key.hashCode();
        items = new MyEntry[defaultSize];
    }

    public MyHashTable(Hasher<K> hasher) {
        this.hasher = hasher;
        items = new MyEntry[defaultSize];
    }

    public MyHashTable(Hasher<K> hasher, int defaultSize) {
        this.hasher = hasher;
        items = new MyEntry[defaultSize];
        this.defaultSize = defaultSize;
    }

    public V put(K key, V v) {
        final int hash = hash(getHashCode(key));
        final int i = indexFor(hash, items.length);
        final double f = loadFactor();
        if (f > 0.75) {
            reCreate();
        }

        if (items[i] == null) {
            items[i] = new MyEntry<>(key, v);
            return v;
        }

        for (MyEntry<K, V> e = items[i]; e != null; e = e.next) {
            if (getHashCode(e.key) == getHashCode(key) && e.key == key || key.equals(e.key)) {
                V oldV = e.value;
                e.value = v;
                return oldV;
            }
        }

        MyEntry<K, V> entry = items[i];
        items[i] = new MyEntry<>(key, v);
        items[i].next = entry;
        entry.previous = items[i];

        return items[i].value;
    }

    public V get(K key) {
        if (key == null)
            return null;
        final int hash = hash(getHashCode(key));
        final int i = indexFor(hash, items.length);

        for (MyEntry<K, V> e = items[i]; e != null; e = e.next) {
            if (getHashCode(e.key) == getHashCode(key) && e.key == key || key.equals(e.key))
                return e.value;
        }
        return null;
    }

    public V remove(K key) {
        if (key == null)
            return null;
        final int hash = hash(getHashCode(key));
        final int i = indexFor(hash, items.length);

        for (MyEntry<K, V> e = items[i]; e != null; e = e.next) {
            if (getHashCode(e.key) == getHashCode(key) && e.key == key || key.equals(e.key)) {
                MyEntry<K, V> p = e.previous;
                MyEntry<K, V> n = e.next;

                if (n == null && p == null) {
                    items[i] = null;
                }

                if (n != null)
                    n.previous = p;
                if (p != null)
                    p.next = n;

                return e.value;
            }
        }
        return null;
    }

    private void reCreate() {
        final MyHashTable<K, V> secondHashTable = new MyHashTable<>(hasher, defaultSize * 2);
        final ArrayList<K> keys = getKeys();
        for (int i = 0; i < keys.size(); i++) {
            secondHashTable.put(keys.get(i), get(keys.get(i)));
        }
        items = secondHashTable.items;
    }

    public int size() {
        size = 0;
        for (int i = 0; i < items.length; i++) {
            for (MyEntry<K, V> e = items[i]; e != null; e = e.next) {
                size++;
            }
        }
        return size;
    }

    public double loadFactor() {
        load = 0;
        for (int i = 0; i < items.length; i++) {
            MyEntry<K, V> e = items[i];
            if (e != null)
                load++;
        }
        return (double) load / (double) items.length;
    }

    public ArrayList<K> getKeys() {
        final ArrayList<K> keys = new ArrayList<>();
        for (int i = 0; i < items.length; i++) {
            for (MyEntry<K, V> e = items[i]; e != null; e = e.next) {
                keys.add(e.key);
            }
        }
        return keys;
    }

    private static int hash(int h) {
        h ^= (h >>> 20) ^ (h >>> 12);
        return h ^ (h >>> 7) ^ (h >>> 4);
    }

    private static int indexFor(int h, int length) {
        return h & (length - 1);
    }

    private int getHashCode(K key) {
        return hasher.hash(key);
    }

    private MyEntry<K, V>[] getItems() {
        return items;
    }
}
