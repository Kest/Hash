package first;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Parser {
    String path;
    SortedSet<String> strings = new TreeSet<>();

    public Parser(String path) {
        this.path = path;
    }

    public void printUniqueWords() throws IOException {
        StringBuffer file = new StringBuffer();
//        Files.lines(Paths.get(path), StandardCharsets.UTF_16).forEach(line -> file.append(line));

        List<String> lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
        for(String line: lines){
            file.append(line);
        }

        String[] split = file.toString().split(" ");

        for (int i = 0; i < split.length; i++) {
            split[i] = split[i].replaceAll("\\pP", "");
            split[i] = split[i].replaceAll("[0-9]", "");
            strings.add(split[i]);
        }

        System.out.println(strings);
    }

    public static void main(String[] args) throws IOException {
        Parser parser = new Parser("/Users/macbook/IdeaProjects/Hash/src/main/resources/words.txt");

        parser.printUniqueWords();
    }
}
