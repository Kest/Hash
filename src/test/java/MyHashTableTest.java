import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import second.MyHashTable;

public class MyHashTableTest extends Assert {

    private static MyHashTable<String, Integer> myHashTable = new MyHashTable<>();

    @BeforeClass
    public static void fillMyHashTable() {
        for (int i = 0; i < 300; i++) {
            assertEquals((Integer) i, myHashTable.put("string" + i, i));
        }
        System.out.println(myHashTable.size());
        System.out.println(myHashTable.loadFactor());
    }

    @Test
    public void shouldAddElement() {

        int size = myHashTable.size();

        assertEquals((Integer) 1000, myHashTable.put("string" + 1000, 1000));
        assertEquals(size + 1, myHashTable.size());
    }

    @Test
    public void shouldRemoveElement() {
        int size = myHashTable.size();
        assertEquals((Integer) 13, myHashTable.remove("string13"));
        System.out.println(myHashTable.get("string13"));
        assertEquals(size - 1, myHashTable.size());
    }

    @Test
    public void shouldGetElement() {
        assertEquals((Integer) 40, myHashTable.get("string40"));
    }

    @Test
    public void shouldReWriteElement() {
        assertEquals((Integer) 33, myHashTable.put("string33", 44));
        assertEquals((Integer) 44, myHashTable.get("string33"));
    }
}
